function errorHandler(err, req, res, next) {
  console.log(err)
  if (err.response.status === 404) {
    // summoner not found
    return res.status(404).json({ message: err.message });
  };

  if (err.response.status === 401) {
    // developmental API key expired
    return res.status(401).json({ message: err.message });
  };

  if (err.response.status === 422) {
    // summoner has not played a game
    return res.status(422).json({ message: err.message });
  };

  return res.status(500).json({ message: err.message });
};

module.exports = errorHandler;