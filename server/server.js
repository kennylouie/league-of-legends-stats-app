const express = require('express');
const app = express();
const cors = require('cors');
const errorHandler = require('./_helpers/error.handler');
require('dotenv').config();

app.use(cors());
app.use('/lol', require('./summoners/summoner.controller'));
app.use(errorHandler);

const server = app.listen(process.env.API_PORT, function(){
  console.log('Listening on port ' + process.env.API_PORT);
});