const axios = require("axios");
require('dotenv').config();

async function searchSummoner(summonerName) {
  return await axios.get(`${process.env.RIOT_API_URL}/lol/summoner/v3/summoners/by-name/${summonerName}?api_key=${process.env.API_KEY}`);
};

async function getMatches(accountId) {
  const matchlistResponse = await getMatchlist(accountId);
  const matchList = matchlistResponse.data;

  let matches = [];
  const repeatNum = Math.min(matchList.matches.length, 10);
  let index = 0;

  for (i = 0; i < repeatNum; i++) {
    const userGameId = matchList.matches[i].gameId;
    const matchDataResponse = await getMatchData(userGameId);
    const matchData = matchDataResponse.data

    const winningTeamIndex = matchData.teams.findIndex(team => team.win == "Win");
    const winningTeamId = matchData.teams[winningTeamIndex].teamId;

    const participantIndex = matchData.participantIdentities.findIndex(participant => participant.player.accountId == accountId);
    const summonerMatchData = matchData.participants[participantIndex];

    // get other summoners in match
    let summoners = [];
    Object.entries(matchData.participantIdentities).forEach(
      ([key, value]) => {
        if (key != participantIndex) {
          const summoner = ({
            name: value.player.summonerName,
            championid: matchData.participants[key].championId
          })
          summoners.push(summoner);
        }
      }
    )

    // creep score
    const creepDeltas = summonerMatchData.timeline.creepsPerMinDeltas // not all games have creepDeltas?
    let creepScore = 0;

    if (creepDeltas) {
      Object.entries(creepDeltas).forEach(
        ([key, value]) => {
          creepScore += value;
      })
      creepScore *= 10
    }

    // match object
    const match = ({
      gamemode: matchData.gameMode,
      gametype: matchData.gameType,
      matchgamecreation: matchData.gameCreation,
      outcome: summonerMatchOutcome(summonerMatchData.teamId, winningTeamId),
      gamelength: matchData.gameDuration,
      champion: summonerMatchData.championId,
      kills: summonerMatchData.stats.kills,
      deaths: summonerMatchData.stats.deaths,
      assists: summonerMatchData.stats.assists,
      championlevel: summonerMatchData.stats.champLevel,
      role: summonerMatchData.timeline.role,
      creepscore: creepScore,
      items: [
        summonerMatchData.stats.item0,
        summonerMatchData.stats.item1,
        summonerMatchData.stats.item2,
        summonerMatchData.stats.item3,
        summonerMatchData.stats.item4,
        summonerMatchData.stats.item5,
        summonerMatchData.stats.item6,
      ],
      gamesummoners: summoners,
      spells: [
        summonerMatchData.spell1Id,
        summonerMatchData.spell2Id,
      ],
    });

    matches.push(match)
    index += 1

    if (index == repeatNum) {
      matches.sort((a, b) => {return b.matchgamecreation - a.matchgamecreation});
      return matches;
    };
  };

};

async function getMatchlist(accountId) {
  return await axios.get(`${process.env.RIOT_API_URL}/lol/match/v3/matchlists/by-account/${accountId}?api_key=${process.env.API_KEY}`);
};

async function getMatchData(matchId) {
  return await axios.get(`${process.env.RIOT_API_URL}/lol/match/v3/matches/${matchId}?api_key=${process.env.API_KEY}`);
};

function summonerMatchOutcome(teamId, winningTeamId) {
  let outcome;
  if (teamId == winningTeamId) {
    outcome = "Victory";
  } else {
    outcome = "Loss";
  };
  return outcome;
};

module.exports = {
  searchSummoner,
  getMatches,
};