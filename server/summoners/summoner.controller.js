const express = require('express');
const router = express.Router();
const summonerService = require('./summoner.service');

router.get('/:summonerName', searchSummoner);

function searchSummoner(req, res, next) {
  summonerService.searchSummoner(req.params.summonerName)
    .then(summonerData => res.json(summonerData.data))
    .catch(err => next(err));
};

router.get('/matches/:accountId', getMatches);

function getMatches(req, res, next) {
  summonerService.getMatches(req.params.accountId)
    .then(matches => {
      res.json(matches);
    })
    .catch(err => next(err));
};

module.exports = router;