import React from 'react';
import { connect } from 'react-redux';

import { userActions } from '../_actions';

class SearchPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      summonerName: ''
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleReset = () => {
    this.setState({
      summonerName: ''
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch } = this.props;
    dispatch(userActions.search(this.state.summonerName));
    this.handleReset();
  };

  render() {

    const { error } = this.props;

    return(
      <div>

        <div className="row">
          <div className="col">
            <nav className="navbar navbar-dark bg-info p-0">
              <a className="navbar-brand pl-3" href="/">
                <img className="navbar-brand" width="35" alt="LOL" src='/League_of_Legends_Icon.png' />
              </a>
            </nav>
          </div>
        </div>

        <div className="row">
          <div className="col-md-6 offset-md-3 col-xs-8 offset-xs-2">
            <div className="row mt-3">
              <div className="col">
                <img className= "main-art" src="/end_01-min.png" alt="gnar"/>
              </div>
            </div>
            <div className="row">
              <form className="input-group mb-5 mt-5" onSubmit={ this.handleSubmit }>
                <input
                value={ this.state.summonerName }
                onChange={ this.handleInputChange }
                name="summonerName"
                className="form-control"
                type="text"
                placeholder="Summoner Name"
                aria-label="Search"
                pattern="^(?!^ +$)[0-9A-Za-z _.]+$"
                />
                <div className="input-group-append">
                  <button className="btn btn-outline-secondary" type="submit" >Find</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        { error &&
        <div className="row">
          <div className="col-md-4 col-sm-12 offset-md-2">
            <img src="/notfound404.png" alt="404"/>
          </div>
          <div className="row align-items-center">
            <div className="col-md-8 col-sm-12">
              <h3 className="text-secondary">Oops summoner not found :(</h3>
              <p className="text-secondary"> {error} </p>
            </div>
          </div>
        </div>
      }

      </div>
    );

  };

};

function mapStateToProps(state) {
  const { error } = state.users;
  return {
    error
  };
};

const connectedSearchPage = connect(mapStateToProps)(SearchPage);
export { connectedSearchPage as SearchPage };