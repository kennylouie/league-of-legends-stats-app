import React from 'react';
import '../styles/styles.css';

const imgSrc="/static/dragontail-8.16.1/8.16.1/img";

export default ({ item: { id, name } }) => {

  return (
    <img className="avatarItem" src={ `${imgSrc}/item/${id}.png` } alt={ name } />
  )

};