import React from 'react';
import '../styles/styles.css';

const imgSrc="/static/dragontail-8.16.1/8.16.1/img";

export default ({ spellData: { id, name, key } }) => {

  return (
    <div className="row">
      <img className="avatarItem" src={ `${imgSrc}/spell/${key}.png` } alt={ name } />
    </div>
  )

};