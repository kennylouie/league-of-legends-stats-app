import React from 'react';
import Item from './Item';
import Summoner from './Summoner';
import Spell from './Spell';

import ChampionsList from '../GameContent/ChampionsList';
import Items from '../GameContent/Items';
import Spells from '../GameContent/Spells';

import '../styles/styles.css';

const imgSrc="/static/dragontail-8.16.1/8.16.1/img";

export default ({ match: {
  gamemode,
  creepscore,
  outcome,
  gamelength,
  champion,
  kills,
  deaths,
  assists,
  championlevel,
  role,
  items,
  gametype,
  matchgamecreation,
  gamesummoners,
  spells,
} }) => {

  // champion name
  let championName;
  Object.entries(ChampionsList.data).forEach(
    ([key, value]) => {
      if (value.id === champion) {
        championName = value.name
      }
    }
  )

  // item data
  let itemsData = [];
  items.forEach(itemId => {
    Object.entries(Items.data).forEach(
      ([key, value]) => {
        if (value.id === itemId) {
          itemsData.push(value)
        };
      }
    );
  });

  // spell data
  let spellsData = [];
  spells.forEach(spellId => {
    Object.entries(Spells.data).forEach(
      ([key, value]) => {
        if (value.id === spellId) {
          spellsData.push(value)
        };
      }
    );
  });

  // summoners splitting
  let gamesummonersA = gamesummoners.slice(0, gamesummoners.length/2 + 1)
  let gamesummonersB = gamesummoners.slice(gamesummoners.length/2 + 1)

  // outcome styling
  const styleOutcome = (outcome) => {
    if (outcome === "Victory") {
      return "text-success"
    } else {
      return "text-danger"
    }
  }

  const bgStyle = (outcome) => {
    if (outcome === "Victory") {
      return "bg-victory"
    } else {
      return "bg-loss"
    }
  }

  // kda
  const kda = (kills, deaths, assists) => {
    if (deaths === 0) {
      return "Perfect"
    }
    return `${((kills + assists)/deaths).toFixed(2)} : 1`
  };

  // time since game started
  const gameTime = (matchgamecreation) => {
    let currentTime = new Date().getTime();
    let timeDelta = (currentTime - matchgamecreation)/1000 - 1320; // not sure about the 1320, but op.gg is always 22 mins (1320 seconds) earlier
    if (timeDelta > 86400) {
      return `${Math.round(timeDelta/86400)} days`
    } else if (timeDelta > 3600) {
      return `${Math.round(timeDelta/3600)} hours`
    } else if (timeDelta > 60) {
      return `${Math.round(timeDelta/60)} mins`
    } else {
      return `${timeDelta} seconds`
    }
  }

  return (
    <div className={`list-group-item mb-2 match-details ${bgStyle(outcome)}`}>
      <div className="row">

        <div className="col-lg-2 col-xs-12 pt-2">
          <p> { gamemode } </p>
          <p> { gametype } </p>
          <p> { gameTime(matchgamecreation) } ago </p>
          <hr />
          <h5 className={styleOutcome(outcome)}>{ outcome }</h5>
          <p className="">{ Math.round( gamelength/60) } mins</p>
        </div>

        <div className="col-lg-1 col-xs-12 pt-2">

          <div className="row">
            <div className="col p-0 mr-4">
              <img className="avatarChampion" src={`${imgSrc}/champion/${championName}.png`} alt={ championName } />
            </div>
          </div>

          <div className="row mt-2">
            <h5>{ championName }</h5>
          </div>

        </div>

        <div className="col-lg-3 col-xs-12 pt-2">
          <div className="row">
            <div className="col-md-4 col-sm-2">
              <div className="col">
                {spellsData.map(spell => {
                  return (
                    <Spell spellData={ spell } key={spell.id} />
                  )
                })}
              </div>
            </div>

            <div className="col-md-8 col-sm-10">
              <p> { role }</p>
              <p>Level: { championlevel }</p>
              <p className="mt-2"><strong>{ kills } / { deaths } / { assists }</strong></p>
              <p><strong>{ kda(kills, deaths, assists) } KDA</strong></p>
              <p className="mt-2">{ Math.round(creepscore) } ( { Math.round(creepscore / Math.round(gamelength/60)) } ) CS</p>
            </div>
          </div>
        </div>

        <div className="col-lg-2 col-xs-12 pl-0 pt-2">
          {itemsData.map(item =>{
            return (
              <Item item={ item } key={item.id} />
            )
          })}
        </div>

        <div className="col-lg-2 col-xs-12">
          {gamesummonersA.map(user => {
            return (
              <Summoner summoner={user} key={user.name}/>
            )
          })}
        </div>

        <div className="col-lg-2 col-xs-12 pr-0">
          {gamesummonersB.map(user => {
            return (
              <Summoner summoner={user} key={user.name}/>
            )
          })}
        </div>

      </div>
    </div>
  );
};