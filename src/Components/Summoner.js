import React from 'react';
import '../styles/styles.css';
import ChampionsList from '../GameContent/ChampionsList';

const imgSrc="/static/dragontail-8.16.1/8.16.1/img";

export default ({ summoner: { name, championid } }) => {

  // champion name
  let championName;
  Object.entries(ChampionsList.data).forEach(
    ([key, value]) => {
      if (value.id === championid) {
        championName = value.name
      }
    }
  )

  return (
    <div className="row pt-0">
      <div className="col-3 p-0">
        <img className="avatarSummoner" src={ `${imgSrc}/champion/${championName}.png` } alt="" />
      </div>
      <div className="col-9 p-0">
        <p className="">{ name }</p>
      </div>
    </div>
  )

};