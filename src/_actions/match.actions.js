import { matchConstants } from '../_constants';
import { matchService } from '../_services';

function getMatches(accountId) {
  return dispatch => {
    dispatch(request());

    matchService.getMatches(accountId)
      .then(
        matches => dispatch(success(matches)),
        error => dispatch(failure(error.toString())),
      );
  };

  function request() { return { type: matchConstants.GETALL_REQUEST } }
  function success(matches) { return { type: matchConstants.GETALL_SUCCESS, matches } }
  function failure(error) { return { type: matchConstants.GETALL_FAILURE, error } }
};

export const matchActions = {
  getMatches,
};