import { userConstants } from '../_constants';
import { userService } from '../_services';
import { history } from '../_helpers';

function search(summonerName) {
  return dispatch => {
    dispatch(request({ summonerName }));

    userService.search(summonerName)
      .then(
        summoner => {
          dispatch(success(summoner.data));
          history.push('/results');
        },
        error => {
          dispatch(failure(error.toString()));
        },
      );
  };

  function request(summoner) { return { type: userConstants.SEARCH_REQUEST, summoner } }
  function success(summonerData) { return { type: userConstants.SEARCH_SUCCESS, summonerData } }
  function failure(error) { return { type: userConstants.SEARCH_FAILURE, error } }
};

export const userActions = {
  search,
};