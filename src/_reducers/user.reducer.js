import { userConstants } from '../_constants';

export function users(state = {}, action) {
  switch(action.type) {
    case userConstants.SEARCH_REQUEST:
      return {
        summoner: action.summoner,
      };
    case userConstants.SEARCH_SUCCESS:
      return {
        summoner: action.summonerData,
      };
    case userConstants.SEARCH_FAILURE:
      return {
        error: action.error,
      };
    default:
      return state;
  }
};