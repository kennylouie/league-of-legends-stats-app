import { matchConstants } from '../_constants';

export function matches(state = {}, action) {
  switch(action.type) {
    case matchConstants.GETALL_REQUEST:
      return {
        loading: true,
      };
    case matchConstants.GETALL_SUCCESS:
      return {
        loading: false,
        matches: action.matches,
      };
    case matchConstants.GETALL_FAILURE:
      return {
        error: action.error,
      };
    default:
      return state;
  }
}