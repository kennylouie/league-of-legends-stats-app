import { combineReducers } from 'redux';

import { users } from './user.reducer';
import { matches } from './match.reducer';

const rootReducer = combineReducers({
  users,
  matches,
});

export default rootReducer;