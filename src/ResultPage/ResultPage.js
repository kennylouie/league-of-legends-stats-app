import React from 'react';
import { connect } from 'react-redux';
import { matchActions } from '../_actions';
import Match from '../Components/Match';

class ResultPage extends React.Component {

  componentDidMount() {
    const { summoner } = this.props;
    this.props.dispatch(matchActions.getMatches(summoner.accountId));
  };

  render() {

    const imgSrc = `/static/dragontail-8.16.1/8.16.1/img`
    const { summoner } = this.props;

    const { loading } = this.props;
    const { error } = this.props;
    const { matches } = this.props;

    return(

      <div>

        <div className="row">
          <div className="col">
            <nav className="navbar navbar-dark bg-info p-0">
              <a className="navbar-brand pl-3" href="/">
                <img className="navbar-brand" width="35" alt="LOL" src='/League_of_Legends_Icon.png' />
              </a>
            </nav>
          </div>
        </div>

        <div className="row">
          <div className="col-md-8 offset-md-2 col-xs-12">
            <div className="mt-5">
              <div className="d-sm-flex">
                <img className="img-thumbnail" src={ `${imgSrc}/profileicon/${summoner.profileIconId}.png` } alt={ summoner.profileIconId } />
                <div className="p-2 ml-3">
                  <h1 className="">{ summoner.name }</h1>
                  <p className="lead"> Level: { summoner.summonerLevel }</p>
                </div>
              </div>
              <hr />
            </div>
          </div>
        </div>

        { loading && <em> Loading matches data </em>}
        { error && <p> no matches data </p> }
        { matches &&
          <div className="list-group mt-5">
            {matches.data.map(match => {
              return (
                <Match match={ match } key={match.matchgamecreation} />
              )
            })}
          </div>
        }

      </div>
    );
  };

};

function mapStateToProps(state) {
  const { summoner } = state.users;
  const { matches, error, loading } = state.matches;
  return {
    summoner,
    matches,
    error,
    loading,
  };
}

const connectedResultPage = connect(mapStateToProps)(ResultPage);
export { connectedResultPage as ResultPage };