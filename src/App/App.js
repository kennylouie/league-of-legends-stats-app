import React from 'react';
import { Router, Route } from 'react-router-dom';

import { history } from '../_helpers';
import { SearchPage } from '../SearchPage';
import { ResultPage } from '../ResultPage';

export class App extends React.Component {

  render() {
    return (
      <div className="container-fluid p-0 bg-light">
        <Router history={history}>
          <div>
            <Route exact path="/" component={ SearchPage } />
            <Route path="/results" component={ ResultPage } />
          </div>
        </Router>
      </div>
    );
  }
};