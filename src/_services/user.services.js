import axios from 'axios';

function search(summonerName) {
  return axios.get(`http://localhost:5000/lol/${summonerName}`)
    .then(summonerData => {
      return summonerData;
    });
};

export const userService = {
  search,
};