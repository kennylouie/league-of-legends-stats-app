import axios from 'axios';

function getMatches(accountId) {
  return axios.get(`http://localhost:5000/lol/matches/${accountId}`)
    .then(summonerMatches => {
      return summonerMatches;
    })
};

export const matchService = {
  getMatches,
};