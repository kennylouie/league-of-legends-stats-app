# League of Legends Stats App

## Introduction

League of legends stats app for displaying latest games data for a player.

## Setup

To get images to be rendered, first download the data dragon static data library
https://ddragon.leagueoflegends.com/cdn/dragontail-8.16.1.tgz

Extract the tar files and place the dragontail-8.16.1 folder into public/static/

Create a .env file in the root of project and add:

```
API_KEY=<your key>

PORT=8000
API_PORT=5000
RIOT_API_URL=https://na1.api.riotgames.com
```

```bash
yarn
nodemon server/server.js
yarn start
```